import sys
import base
import base_driver
import base_desktop_environment

print('Step 5: Install and configure graphic interface.')

'''
CHECK NETWORK
'''
if not base.is_network_accessible():
    print('Unable to update packs database. Please check your network connection.')
    sys.exit()

'''
Selection
'''


def do_select(selections: tuple[base.Selection]) -> list[base.Selection]:
    # Select
    for index in range(0, len(selections)):
        selection = selections[index]
        print(f"\t{index + 1}: {selection.get_name()}")
    print('Input the relevant number(s) and press Enter to install (separated by space(s)) (default:1):', end='')
    opt_strs = input().split()

    opts: set[int] = set()
    for opt_str in opt_strs:
        if opt_str.isdigit():
            opt = int(opt_str) - 1
            if opt in range(0, len(selections)):
                opts.add(opt)
    if (len(opts)) == 0:
        opts.add(0)

    selections_ret: list[base.Selection] = []
    to_be_installed_str = ''
    for opt in opts:
        selections_ret.append(selections[opt])
        to_be_installed_str += f"\'{selections[opt].get_name()}\'"

    # Confirm
    print()
    print(f'Confirm to install {to_be_installed_str}? [Y/n]', end='')
    if base.input_yes_or_no(True):
        return selections_ret
    else:
        return []


'''
Install
'''


def install_selections(selections: list[base.Selection]):
    # Install
    for selection in selections:
        print(f'Installing {selection.get_name()}...')
        selection.inst()


'''
Main
'''
selections = do_select(base_driver.selections) + do_select(base_desktop_environment.selections)
install_selections(selections)

'''
End
'''
print('Don\'t forget to configure non-root users(the next step).')
