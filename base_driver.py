import base

print('Step 5: Install the driver compatible with your computer.')


def inst_intel() -> None:
    # 1-driver
    base.pacman_inst('mesa xf86-video-intel intel-gpu-tools')
    # 2-opencl
    base.pacman_inst('intel-compute-runtime intel-graphics-compiler \
        intel-opencl-clang ocl-icd opencl-headers')
    # 3-vulkan
    base.pacman_inst('vulkan-icd-loader vulkan-intel \
        vulkan-swrast vulkan-tools')
    # 4-videoacceleration
    base.pacman_inst('intel-media-driver libva-intel-driver \
        intel-media-sdk')


def inst_amdgpu() -> None:
    # 1-driver
    base.pacman_inst('mesa xf86-video-amdgpu radeontop')
    # 2-opencl
    # base.pacman_inst('opencl-mesa ocl-icd opencl-headers')
    # 3-vulkan
    base.pacman_inst('vulkan-icd-loader vulkan-radeon \
        vulkan-swrast vulkan-tools')
    # 4-videoacceleration
    base.pacman_inst('libva-mesa-driver mesa-vdpau')
    # 5-rocm
    # base.pacman_inst('rocm-hip-sdk')


def inst_nvidia() -> None:
    # 1-driver
    base.pacman_inst('nvidia-utils nvidia-dkms nvidia-settings')
    # 2-opencl
    base.pacman_inst('opencl-nvidia ocl-icd opencl-headers')
    # 3-vulkan
    base.pacman_inst('vulkan-icd-loader \
        vulkan-swrast vulkan-tools')
    # 4-videoacceleration
    # (packed by nvidia-utils)


def inst_nvidia_optimus() -> None:
    inst_nvidia()
    base.pacman_inst('optimus-manager')
    base.run('systemctl enable optimus-manager')


def inst_vmware() -> None:
    base.pacman_inst('open-vm-tools gtkmm gtkmm3 libxtst asp',
                     'mesa xf86-input-libinput xf86-video-vmware',
                     'vulkan-icd-loader vulkan-swrast vulkan-tools')
    base.add_module('vmw_balloon vmw_pvscsi vmw_vmci vmwgfx vmxnet3',
                    'vsock vmw_vsock_vmci_transport')
    base.run('mkinitcpio -P')
    base.run('systemctl enable vmtoolsd')
    base.run('systemctl enable vmware-vmblock-fuse')


def inst_virtualbox() -> None:
    base.pacman_inst('virtualbox-guest-utils',
                     'vulkan-icd-loader vulkan-swrast vulkan-tools')
    base.run('systemctl enable vboxservice')


def inst_vulkan_devel() -> None:
    base.pacman_inst('vulkan-devel glfw-doc glm')


class DriverSelection(base.Selection):
    name = 'No name'
    function = None

    def __init__(self, name, function):
        self.name = name
        self.function = function

    def get_name(self) -> str:
        return self.name

    def inst(self) -> None:
        self.function()


def skip() -> None:
    pass


selections = (
    DriverSelection('AMDGPU', inst_amdgpu),
    DriverSelection('Intel Graphics', inst_intel),
    DriverSelection('NVIDIA', inst_nvidia),
    DriverSelection('NVIDIA Optimus', inst_nvidia_optimus),
    DriverSelection('Vmware Guest', inst_vmware),
    DriverSelection('VirtualBox Guest', inst_virtualbox),
    DriverSelection('Optional: For Vulkan Developer', inst_vulkan_devel),
    DriverSelection('Skip', skip)
)
