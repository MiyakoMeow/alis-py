import base

"""
Functions
"""


# --- Base Desktop Engine
def _inst_base_xorg() -> None:
    base.pacman_remove('glfw-wayland')
    base.pacman_inst('xorg mesa-demos',
                     'gtk3 gtk4 qt5ct qt5-base qt6-base',
                     'clutter glfw-x11 glew wlroots')
    base.pacman_remove('vim')
    base.pacman_inst('gvim')


def _inst_base_wayland() -> None:
    base.pacman_remove('xorg', 'glfw-x11')
    base.pacman_inst('wayland xorg-xwayland mesa-demos',
                     'gtk3 gtk4 qt5ct qt5-wayland qt6-wayland',
                     'clutter glfw-wayland glew wlroots')
    base.pacman_remove('vim')
    base.pacman_inst('gvim')


# --- Base ---
def _inst_alsa() -> None:
    base.pacman_inst('alsa-utils alsa-plugins alsa-oss alsa-firmware alsa')
    base.run('systemctl enable alsa-restore')
    base.run('systemctl enable alsa-state')


def _inst_pipewire() -> None:
    base.pacman_inst('pipewire pipewire-docs lib32-pipewire pipewire-zeroconf pipewire-v4l2 noise-suppression-for-voice',
                     'wireplumber pqwgraph helvum pipewire-audio pipewire-alsa pipewire-jack lib32-pipewire-jack pipewire-pulse carla'
                     'easyeffects alsa-card-profiles',)


def _inst_bluez() -> None:
    base.pacman_inst('bluez bluez-utils pulseaudio-bluetooth bluez-cups bluez-hid2hci')
    base.run('systemctl enable bluetooth')
    base.add_module('btusb')


# --- Desktop Usage ---
def _inst_ttf() -> None:
    base.pacman_inst('ttf-fira-code wqy-zenhei',
                     'ttf-symbola ttf-nerd-fonts-symbols powerline-fonts',
                     'noto-fonts-cjk noto-fonts-emoji ttf-font-awesome')
    base.pacman_inst('nerd-fonts-complete') # From archlinuxcn


def _inst_develop() -> None:
    base.pacman_inst('ninja cmake sqlite mpdecimal tk',
                     'python-pip python-pipenv-to-requirements',
                     'python-pipenv python-pipreqs python-requirementslib',
                     'jdk-openjdk openjdk-doc openjdk-src',
                     'java-openjfx java-openjfx-doc java-openjfx-src',
                     'alure freealut gambas3-gb-openal openal-examples',
                     'code neovide',
                     'rustup',
                     'nvm',
                     'portaudio lib32-portaudio',)


def _inst_octopi() -> None:
    base.pacman_inst('octopi')


# --- Input
def _inst_fcitx() -> None:
    base.pacman_inst('fcitx5 fcitx5-chinese-addons',
                     'fcitx5-gtk fcitx5-qt fcitx5-configtool fcitx5-material-color')
    base.pacman_inst('fcitx5-pinyin-zhwiki fcitx5-pinyin-moegirl')
    input_engine = 'fcitx5'
    profile = open('/etc/profile', mode='a')
    profile.write(f'''
    export INPUT_METHOD={input_engine}
    export GTK_IM_MODULE={input_engine}
    export QT_IM_MODULE={input_engine}
    export XMODIFIERS=\"@im={input_engine}\"
    ''')
    profile.close()


def _inst_ibus() -> None:
    base.pacman_inst('ibus dconf-editor ibus-libpinyin')
    # environment
    input_engine = 'ibus'
    profile = open('/etc/profile', mode='a')
    profile.write(f'''
    export XIM_PROGRAM={input_engine}
    export XIM={input_engine}
    export GTK_IM_MODULE={input_engine}
    export QT_IM_MODULE={input_engine}
    export XMODIFIERS=\"@im={input_engine}\"
    ''')
    profile.close()


# --- Software
def _inst_soft() -> None:
    base.pacman_inst('vlc firefox netease-cloud-music',
                     'obs-studio',
                     )
    base.pacman_inst('wine wine-gecko wine-mono \
        giflib libpng libldap gnutls mpg123 \
        openal v4l-utils libpulse alsa-plugins alsa-lib \
        libjpeg-turbo libxcomposite libxinerama \
        opencl-icd-loader libxslt gst-plugins-base-libs \
        vkd3d sdl2 libgphoto2 sane gsm cups samba dosbox')


# --- Display manager
def _apply_dm_gdm() -> None:
    base.run('systemctl disable display')
    base.pacman_inst('gdm')
    base.run('systemctl enable gdm')


def _apply_dm_lightdm() -> None:
    base.run('systemctl disable display')
    base.pacman_inst('lightdm')
    base.run('systemctl enable lightdm')


def _apply_dm_sddm() -> None:
    base.run('systemctl disable display')
    base.pacman_inst('sddm')
    base.run('systemctl enable sddm')


def _apply_dm_sddm_wayland() -> None:
    base.run('systemctl disable display')
    base.pacman_inst('sddm weston')
    base.run('systemctl enable sddm')
    base.run("mkdir -p /etc/sddm.conf.d")
    base.run("cp ./files/sddm.conf.d/10-wayland.conf /etc/sddm.conf.d")


# --- Desktop
def _inst_plasma() -> None:
    base.pacman_inst('plasma kde-applications', 'kimtoy')
    base.pacman_remove('kde-games')
    base.pacman_remove(
        'dragon elisa gwenview kate yakuake okular kwrite',
        'falkon konqueror')
    _inst_alsa()
    _inst_bluez()
    _inst_ttf()
    _inst_fcitx()
    _inst_octopi()
    _inst_develop()
    _inst_soft()
    # _apply_dm_sddm() # Not included


def inst_desktop_plasma_xorg() -> None:
    _inst_base_xorg()
    _inst_plasma()
    _apply_dm_sddm()


def inst_desktop_plasma_wayland() -> None:
    _inst_base_wayland()
    _inst_plasma()
    base.pacman_inst('plasma-wayland-session egl-wayland')
    _apply_dm_sddm_wayland()


def inst_desktop_gnome_wayland() -> None:
    _inst_base_wayland()
    base.pacman_inst('gnome gnome-extra webp-pixbuf-loader')
    _inst_alsa()
    _inst_bluez()
    _inst_ttf()
    _inst_ibus()
    _inst_octopi()
    _inst_develop()
    _inst_soft()
    _apply_dm_gdm()


def inst_desktop_i3_xorg() -> None:
    _inst_base_xorg()
    base.pacman_inst('i3 feh picom rofi pcmanfm gvfs',
                     'xarchiver terminator network-manager-applet',
                     'arc-gtk-theme librsvg lxappearance')
    base.pacman_inst('polybar')
    _inst_alsa()
    _inst_bluez()
    _inst_ttf()
    _inst_fcitx()
    _inst_octopi()
    _inst_develop()
    _inst_soft()
    _apply_dm_sddm()


def inst_desktop_sway() -> None:
    _inst_base_wayland()
    base.pacman_inst('sway swaylock swayidle wofi swaybg wlroots alacritty',
                     'pcmanfm-qt gvfs xarchiver waybar',
                     'gpicview lxtask network-manager-applet',
                     'xdg-desktop-portal-wlr',
                     'arc-gtk-theme librsvg lxappearance',)
    _inst_pipewire()
    _inst_bluez()
    _inst_ttf()
    _inst_fcitx()
    _inst_octopi()
    _inst_develop()
    _inst_soft()
    _apply_dm_sddm_wayland()

def inst_desktop_hyprland() -> None:
    _inst_base_wayland()
    base.pacman_inst('hyprland wofi wlroots waybar alacritty',
                     'thunar gvfs gvfs-mtp gvfs-smb sshfs exo catfish mlocate zeitgeist clamav clamtk',
                     'thunar-archive-plugin thunar-media-tags-plugin udiskie thunar-volman tumbler libgsf',
                     'gpicview lxtask network-manager-applet',
                     'xdg-desktop-portal-hyprland',
                     'xfce-polkit',
                     'arc-gtk-theme librsvg lxappearance',)
    _inst_pipewire()
    _inst_bluez()
    _inst_ttf()
    _inst_fcitx()
    _inst_octopi()
    _inst_develop()
    _inst_soft()
    _apply_dm_sddm_wayland()


def inst_desktop_dde() -> None:
    _inst_base_xorg()
    base.pacman_inst('deepin deepin-extra')
    _inst_alsa()
    _inst_bluez()
    _inst_ttf()
    _inst_fcitx()
    _inst_octopi()
    _inst_develop()
    _inst_soft()
    _apply_dm_lightdm()


'''
Selecting
'''


class DesktopEnvironment(base.Selection):
    name: str = 'No name'
    function = None

    def __init__(self, name, function):
        self.name = name
        self.function = function

    def get_name(self) -> str:
        return self.name

    def inst(self) -> None:
        self.function()


def skip() -> None:
    pass


selections = (
    DesktopEnvironment('KDE Plasma (Wayland)', inst_desktop_plasma_wayland),
    DesktopEnvironment('Gnome (Wayland)', inst_desktop_gnome_wayland),
    DesktopEnvironment('Sway (Wayland)', inst_desktop_sway),
    DesktopEnvironment('Hyprland (Wayland)', inst_desktop_hyprland),
    DesktopEnvironment('KDE Plasma (Xorg)', inst_desktop_plasma_xorg),
    DesktopEnvironment('i3wm (Xorg)', inst_desktop_i3_xorg),
    DesktopEnvironment('Skip', skip)
)
