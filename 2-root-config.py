import sys

import base

print('Step 2: Advanced configure.')

'''
CHECK NETWORK
'''

if not base.is_network_accessible():
    print('Unable to update packs database. Please check your network connection.')
    sys.exit()

'''
CONFIGURATIONS
'''
# Host
print()
host_name = input('Input the hostname of new run(not username)(e.g. ZhangSan\'s PC):')
if len(host_name) == 0:
    host_name = 'ArchLinux'

host_name_file = open('/etc/hostname', mode='w')
host_name_file.write(host_name)
host_name_file.close()
hosts_file = open('/etc/hosts', mode='a')
hosts_file.write(f"""
127.0.0.1 localhost
::1 localhost
127.0.1.1 {host_name}.localdomain {host_name}""")
hosts_file.close()

# Root passward
print()
print('Setting root user\'s passward (VERY IMPORTANT TO BE REMEMBERED!!!)')

is_password_set = False
while not is_password_set:
    is_password_set = (base.run('passwd') == 0)

# Locale and fonts
print()
print('Configuring locales...')

base.run('locale-gen')
base.run('chsh -s /usr/bin/fish')

# Network
base.run('systemctl enable NetworkManager')

print(f'Use iwd as NetworkManager wireless backend? [Y/n]', end='')
if base.input_yes_or_no(True):
    base.run('systemctl enable iwd')
    hosts_file = open('/etc/NetworkManager/NetworkManager.conf', mode='a')
    hosts_file.write(f"""[device]
    wifi.backend=iwd""")
    hosts_file.close()

archlinuxcn_result = base.pacman_inst('paru')
if archlinuxcn_result == 0:
    base.pacman_remove('vi vim')
    base.pacman_inst('neovim-symlinks')