import base

# Show
base.run('lsblk')

# Operate
print("""
sdx1 -> ntfs, ARCHDD
sdx2 -> vfat(efi)
sdx3 -> swap
sdx4 -> ext4, ARCHRT
""")
disk = input("Input the disk to operate(e.g. sda):")

print("Confirm to Operate?[y/N]")
if base.input_yes_or_no(False):
    base.run(f'mkfs.ntfs --fast --force --label ARCHDD /dev/{disk}1')
    base.run(f'mkfs.vfat -F 32 /dev/{disk}2')
    base.run(f'mkswap /dev/{disk}3')
    base.run(f'mkfs.btrfs -f -L ARCHRT /dev/{disk}4')
    # Mount
    base.run(f'mount /dev/{disk}4 /mnt')
    base.run(f'mkdir /mnt/boot')
    base.run(f'mkdir /mnt/boot/efi')
    base.run(f'mount /dev/{disk}2 /mnt/boot/efi')
    base.run(f'swapon /dev/{disk}3')
    base.run(f'mkdir /mnt/extra-hdd')
    base.run(f'mount /dev/{disk}1 /mnt/extra-hdd')