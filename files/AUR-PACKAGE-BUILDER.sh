#!/bin/sh
echo "WELCOME to AUR PACKAGES MAKER!!!"
echo "Author: MTE (bilibili:MTE001)"
echo ""
printf "Input the AUR git you want to clone:"
read pack

do_build() {
	echo "Start to make ${pack}..."
	cd ./${pack}
	makepkg -s
	mv ./*.pkg.tar* ./..
}

do_prepare() {
	firefox https://aur.archlinux.org/packages/${pack}/
}

case ${pack} in
	"")
		echo "No git is going to be clone."
		;;
	*)
		echo "Start to clone ${pack}.git..."
		git clone https://aur.archlinux.org/${pack}.git
		echo "Clone success."
		# prepare
		echo ""
		printf "Start building or prepare?[Y/p/n]:"
		read opt
		case ${opt} in
			""|Y*|y*)
				do_build
				;;
			p*|P*)
				do_prepare
				;;
		esac
		;;
esac
