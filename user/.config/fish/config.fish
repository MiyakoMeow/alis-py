if status is-interactive
    # Commands to run in interactive sessions can go here
end

load_nvm > /dev/stderr

set -x PATH $PATH /opt/rocm/bin
set -x PATH $PATH ~/.local/bin

# ROS 2
set ROS_DOMAIN_ID 42
bass source /opt/ros/humble/setup.bash
